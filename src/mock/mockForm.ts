export interface Form {
    bio: string;
    rating: string;
    accreditation: boolean;
    amount: number;
    category: string;
    comment: string;
    files: File[]
}

export type KeysList =
    'bio' |
    'rating' |
    'accreditation' |
    'amount' |
    'category' |
    'comment' |
    'files';

export const FormValues: Record<keyof Form, KeysList> = {
    bio: 'bio',
    rating: 'rating',
    accreditation: 'accreditation',
    amount: 'amount',
    category: 'category',
    comment: 'comment',
    files: 'files'
};