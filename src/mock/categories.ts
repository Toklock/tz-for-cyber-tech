export const categories = [
    {
        id: 1,
        name: 'Категория 1',
        value: 'category 1',
    },
    {
        id: 2,
        name: 'Категория 2',
        value: 'category 2',
    },
    {
        id: 3,
        name: 'Категория 3',
        value: 'category 3',
    },
    {
        id: 4,
        name: 'Категория 4',
        value: 'category 4',
    },
    {
        id: 5,
        name: 'Категория 5',
        value: 'category 5',
    },
]