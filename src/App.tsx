import React from 'react';
import styled from "styled-components";
import {From} from "./components/Form";
import './styles/globals.css'
import './styles/reset.css'
import './styles/vars.css'

export default function App() {
    return (
        <Wrapper className="app">
            <From/>
        </Wrapper>
    );
}

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
`
