import styled from "styled-components";
import {useFormContext} from "react-hook-form";
import {ErrorHint} from "./ErrorHint";

interface Props {
    required?: boolean;
    options: Record<string, any>;
    placeholder: string;
    name: string;
}

export const Input = ({placeholder, name, options, required}: Props) => {
    const { register, formState: { errors } } = useFormContext();

    return (
        <Wrapper className="inputWrapper">
            <input
                className={`inputWrapper__input ${errors[name] ? "inputWrapper__input_error" : ""}`}
                placeholder={placeholder}
                type="text"
                {...register(name, {required, ...options})}
            />
            {errors[name] && <ErrorHint>Введите корректное значение</ErrorHint>}
        </Wrapper>
    )
}

const Wrapper = styled.div`
    .inputWrapper__input {
        padding: 10px 12px;
        font-size: 16px;
        line-height: 24px;
        border-radius: 6px;
        border: 1px solid var(--white);
        caret-color: var(--green);

        &::placeholder {
            color: var(--gray);
        }

        &:focus {
            border-color: var(--green);
        }
        
        &.inputWrapper__input_error {
            border-color: var(--red);
        }
    }
`
