import {useEffect, useState} from "react";
import {FormProvider, SubmitHandler, useForm} from "react-hook-form";
import styled from "styled-components";
import {Input} from "./Input";
import {Label} from "./Label";
import {Button} from "./Button";
import {Select} from "./Select";
import {Checkbox} from "./Checkbox";
import {DropZone} from "./DropZone";
import {Form, FormValues} from "../mock/mockForm";
import {categories} from "../mock/categories";

export const From = () => {
    const [total, setTotal] = useState(0);
    const submit: SubmitHandler<Form> = (form) => sendForm(form);
    const methods = useForm<Form>({defaultValues: {amount: 0}});
    const [amount, accreditation] = methods.watch(['amount', 'accreditation'])

    const sendForm = (form: Form) => {
        console.log('FORM', form)
    }

    useEffect(() => {
        const num = Number(amount);
        setTotal(accreditation ? num + (num / 100 * 20) : num);
    }, [amount, accreditation])

    return (
        <Wrapper>
            <FormProvider {...methods}>
                <form>
                    <h2>Заполните форму</h2>
                    <Label title={'ФИО'} required>
                        <Input
                            placeholder={'Заполнить'}
                            name={FormValues.bio}
                            options={{minLength: 10, maxLength: 30}}
                            required
                        />
                    </Label>
                    <Label title={'Рейтинг'} required>
                        <Input
                            placeholder={'Введите значение от 1 до 100'}
                            name={FormValues.rating}
                            options={{min: 1, max: 100}}
                            required
                        />
                    </Label>

                    <Checkbox title={'Имеется аккредитация'} name={FormValues.accreditation}/>

                    <Label title={'Желаемая сумма'} required>
                        <Input
                            placeholder={'0'}
                            name={FormValues.amount}
                            options={{pattern: /^[1-9][0-9]{5}[.,][0-9]{2}$/i,}}
                            required
                        />
                    </Label>

                    <Label title={'Категория'} required>
                        <Select options={categories} name={FormValues.category} required/>
                    </Label>

                    <Label title={'Комментарий'}>
                        <Input
                            placeholder={'Заполнить'}
                            name={FormValues.comment}
                            options={{maxLength: 200}}
                        />
                    </Label>

                    <DropZone name={FormValues.files}/>

                    <div className={'form__total'}>Итоговая сумма {total}</div>

                    <Button onClick={methods.handleSubmit(submit)}>Отправить</Button>
                </form>
            </FormProvider>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    background-color: var(--background);
    border-radius: 12px;
    padding: 24px;
    width: 451px;
    margin: 0 auto;

    h2 {
        font-size: 28px;
        line-height: 24px;
        margin-bottom: 33px;
    }

    .form__total {
        margin: 21px 0;
        font-size: 20px;
        line-height: 24px;
    }
`
