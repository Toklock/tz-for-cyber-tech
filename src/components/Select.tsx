import styled from "styled-components";
import {useFormContext} from "react-hook-form";
import {ErrorHint} from "./ErrorHint";

interface Props {
    options: any[];
    name: string;
    required?: boolean;
}

export const Select = ({options, name, required}: Props) => {
    const {register, formState: {errors}} = useFormContext();

    return (
        <Wrapper className={'select'}>
            <select {...register(name, {required})}>
                <option disabled value="default">Выбрать</option>
                {options.map(option => (<option key={option.id} value={option.value}>{option.name}</option>))}
            </select>
            {errors[name] && <ErrorHint>Выберите значение</ErrorHint>}
        </Wrapper>
    )
}

const Wrapper = styled.div`
    display: flex;

    .select__option {
        padding: 10px 12px;
        font-size: 16px;
        line-height: 24px;
        border-radius: 6px;
        display: block;
    }
`
