import {MouseEventHandler, ReactNode} from "react";
import styled from "styled-components";

interface Props {
    children: ReactNode;
    onClick: MouseEventHandler;
}

export const Button = ({children, onClick}: Props) => {
    return (
        <Wrapper onClick={onClick}>
            {children}
        </Wrapper>
    )
}

const Wrapper = styled.button`
    background-color: var(--gray);
    color: var(--white);
    padding: 8px 16px;
    font-size: 16px;
    line-height: 24px;
    border-radius: 40px;
    width: 100%;
    text-align: center;

    &:hover {
        background-color: var(--green);
    }
`
