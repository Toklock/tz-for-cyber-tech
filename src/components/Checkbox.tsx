import styled from "styled-components";
import {useFormContext} from "react-hook-form";
import {ReactComponent as CheckIcon} from "../icons/check.svg";

interface Props {
    title: string;
    name: string;
}

export const Checkbox = ({title, name}: Props) => {
    const {register} = useFormContext();

    return (
        <Wrapper className="checkbox">
            <input
                type="checkbox"
                className="checkbox__input"
                {...register(name)}
            />
            <div className={'checkbox__check'}>
                <CheckIcon/>
            </div>
            <div className="checkbox__title">
                {title}
            </div>
        </Wrapper>
    )
}

const Wrapper = styled.label`
    margin-bottom: 21px;
    display: flex;
    align-items: center;

    .checkbox__check {
        width: 16px;
        height: 16px;
        border: 1px solid var(--gray);
        border-radius: 4px;
        display: flex;
        align-items: center;
        justify-content: center;

        svg {
            opacity: 0;
        }
    }

    .checkbox__input {
        visibility: hidden;
        position: absolute;
        width: 1px;
        height: 1px;

        &:checked + .checkbox__check {
            background: var(--green);
            border-color: var(--green);

            svg {
                opacity: 1;
            }
        }
    }

    .checkbox__title {
        color: var(--black);
        margin-left: 8px;
        font-size: 16px;
        line-height: 24px;
        user-select: none;
    }
`
