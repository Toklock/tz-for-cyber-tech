import {ReactNode} from "react";
import styled from "styled-components";

interface Props {
    title: string;
    required?: boolean;
    children: ReactNode;
}

export const Label = ({title, required, children}: Props) => {
    return (
        <Wrapper className="label">
            <div className="label__title">
                {title}
                {required
                    ? <span className="label__star">*</span>
                    : ''
                }
            </div>
            {children}
        </Wrapper>
    )
}

const Wrapper = styled.label`
    display: block;
    margin-bottom: 21px;
    width: inherit;

    .label__title {
        color: var(--gray);
        margin-bottom: 4px;
        font-size: 14px;
        line-height: 18px;
    }

    .label__star {
        color: var(--red);
    }
`
