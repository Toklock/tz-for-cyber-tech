import React from "react";
import styled from "styled-components";
import {useFormContext} from "react-hook-form";
import {ReactComponent as File} from "../icons/File.svg";
import {ReactComponent as Cross} from "../icons/cross.svg";
import {Label} from "./Label";

interface Props {
    name: string;
}

export const DropZone = ({name}: Props) => {
    const {setValue, watch} = useFormContext();
    const files = watch('files');

    function dropFile(e: any) {
        e.preventDefault();
        const files = [...e.dataTransfer.files];
        setValue(name, files)
    }

    function onCross(e: React.MouseEvent, i: number) {
        e.preventDefault();
        const copy = [...files]
        copy.splice(i, 1)
        setValue(name, copy)
    }

    function onFileChange(e: React.ChangeEvent<HTMLInputElement>) {
        const files = e.target.files;
        if (!files || !files.length) return;

        setValue(name, [...files])
    }

    const fileItems = files?.length > 0 && files?.map((file: File, index: number) => {
        return (
            <div key={file.lastModified} className={'drop-zone__file'}>
                <div>
                    <File className={'drop-zone__icon'}/>
                    {file.name}
                </div>
                <div>
                    <div>{new Date(file.lastModified).toDateString()}</div>
                    <Cross
                        onClick={(e) => onCross(e, index)}
                        className={'drop-zone__cross'}
                    />
                </div>
            </div>
        )
    })

    return (
        <Wrapper className="drop-zone">
            <Label title={'Загрузите файл'}>
                {
                    files?.length > 0
                        ? <div className="drop-zone__files">
                            {fileItems}
                        </div>
                        : <div
                            onDragOver={e => e.preventDefault()}
                            onDrop={dropFile}
                            className={'drop-zone__area'}
                        >
                            Нажмите на область или перетащите файлы сюда.
                            5 МБ максимальный размер
                            <div className={'drop-zone__green'}>Выбрать файл</div>
                            (pdf)
                        </div>
                }

                <input
                    onChange={onFileChange}
                    style={{display: 'none'}}
                    multiple
                    type="file"
                />
            </Label>
        </Wrapper>
    )
}

const Wrapper = styled.div`
    display: flex;
    color: var(--gray);
    width: 100%;

    .drop-zone__files {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: space-between;
        padding: 10px 12px;
        font-size: 16px;
        line-height: 24px;
        border-radius: 6px;
        background-color: var(--white);
    }

    .drop-zone__file {
        display: flex;

        > div {
            display: flex;
        }
    }

    .drop-zone__icon {
        margin-right: 6px;
        min-width: 24px;
    }

    .drop-zone__cross {
        margin-left: 16px;
        min-width: 24px;
    }

    .drop-zone__area {
        border-radius: 8px;
        border: 2px dashed var(--gray);
        padding: 8px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        text-align: center;
    }

    .drop-zone__green {
        color: var(--green);
        margin: 4px 0;
    }
`
