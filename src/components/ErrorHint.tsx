import {ReactNode} from "react";
import styled from "styled-components";

interface Props {
    children: ReactNode;
}

export const ErrorHint = ({children}: Props) => {
    return (
        <Wrapper className="error">
            {children}
        </Wrapper>
    )
}

const Wrapper = styled.div`
    color: var(--red);
    margin-top: 2px;
`
